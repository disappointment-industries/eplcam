package space.rethelyi.eplcam

import android.Manifest
import android.os.Environment
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionStatus
import com.google.accompanist.permissions.rememberPermissionState
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun photoTaker(text: String): ()->Unit {
    val context = LocalContext.current

    val dir = File(context.cacheDir, "images")
    dir.mkdirs()

    val file = File(dir, "image.jpg")


    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.TakePicture()) {
        val txt = if (it) {
            val tgtDir = File(
                Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DCIM
                ), "Expleo"
            )
            tgtDir.mkdirs()
            val timestamp: String = SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US).format(Date())
            val newFile = File(tgtDir, "$text-$timestamp.jpg")
            file.copyTo(newFile, overwrite = true)
            file.delete()
            "Photo saved"
        } else {
            "Photo cancelled"
        }

        Toast.makeText(context, txt, Toast.LENGTH_SHORT).show()
    }

    val cameraPermissionState = rememberPermissionState(
        Manifest.permission.CAMERA
    )

    return {
        when {
            cameraPermissionState.status is PermissionStatus.Denied -> {
                Toast.makeText(context, "The camera is important for this app. Please grant the permission.", Toast.LENGTH_LONG).show()
                cameraPermissionState.launchPermissionRequest()
            }
            cameraPermissionState.status != PermissionStatus.Granted -> cameraPermissionState.launchPermissionRequest()
            else -> {
                val uri = Provider.getImageUri(context, file)
                launcher.launch(uri)
            }
        }
    }
}
