package space.rethelyi.eplcam

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import java.io.File

class Provider: FileProvider(R.xml.file_paths) {
    companion object {
        fun getImageUri(context: Context, file: File): Uri {
            val authority = context.packageName + ".fileprovider"
            return getUriForFile(
                context,
                authority,
                file,
            )
        }
    }
}