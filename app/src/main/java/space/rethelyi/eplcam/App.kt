package space.rethelyi.eplcam

import android.content.Context
import android.content.res.Configuration
import android.view.KeyEvent.KEYCODE_ENTER
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.key.onKeyEvent
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp

private const val prefKey = "lastText"

@Composable
fun App() {
    val context = LocalContext.current
    val prefereces = remember { context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE) }

    var text by remember {
        mutableStateOf(prefereces.getString(prefKey, null) ?: "Gyáriszám")
    }

    val taker = photoTaker(text)
    val onClick = {
        taker()
        prefereces.edit().putString(prefKey, text).apply()
    }

    val configuration = LocalConfiguration.current

    val content = @Composable { camModifier: Modifier, textModifier: Modifier ->
        CameraButton(
            onClick,
            modifier = camModifier,
        )
        OutlinedTextField(
            value = text,
            onValueChange = { text = it},
            textStyle =  MaterialTheme.typography.h2,
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Done,
            ),
            keyboardActions = KeyboardActions(onDone = { onClick() }),
            modifier = textModifier
                .onKeyEvent {
                    if (it.nativeKeyEvent.keyCode == KEYCODE_ENTER) {
                        onClick()
                        true
                    } else {
                        false
                    }
                }
        )
        CameraButton(
            onClick,
            modifier = camModifier,
        )
    }

    when (configuration.orientation) {
        Configuration.ORIENTATION_LANDSCAPE -> {
            Row(
                modifier = Modifier
                    .fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly,
            ) {
                val mod = Modifier.weight(1f).fillMaxSize()
                content(
                    mod.padding(vertical = 84.dp, horizontal = 32.dp),
                    mod,
                )
            }
        }
        else -> {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.SpaceEvenly,
            ) {
                val mod = Modifier.weight(1f).fillMaxSize()
                content(
                    mod.padding(horizontal = 84.dp, vertical = 32.dp),
                    mod,
                )
            }
        }
    }

}

@Composable
fun CameraButton(
    onClicked: () -> Unit,
    modifier: Modifier,
) {
    Button(
        onClick = { onClicked() },
        modifier = modifier
    ) {
        Text(
            text = "Camera",
            style =  MaterialTheme.typography.h4,
        )
    }
}
