package space.rethelyi.eplcam

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class SharedPref(private val context: Context) {
    companion object {
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore("textInput")
        val KEY = stringPreferencesKey("text")
    }

    val getText: Flow<String?> = context.dataStore.data
        .map { preferences ->
            preferences[KEY] ?: "Gyáriszám"
        }

    suspend fun saveText(text: String) {
        context.dataStore.edit { preferences ->
            preferences[KEY] = text
        }
    }
}
